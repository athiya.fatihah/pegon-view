# run this script with sudo bash deploy-fe.sh
# use the -x flag if something is confusing
echo "Pulling from main..."
git pull origin main
echo "Building image..."
docker build . -t admin/pegonizer-fe
unset DOCKER_ID
echo "Checking running containers..."
DOCKER_ID=$(docker ps -aqf "name=pegonizer-fe")
[ ! -z $DOCKER_ID ] && docker stop "$DOCKER_ID" && docker rm "$DOCKER_ID"
echo "Deploying..."
docker run -p 8080:80 --name pegonizer-fe -d admin/pegonizer-fe
