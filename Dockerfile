# adapted from https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/kelas-c/peer-review-kepangkatan-akademik-pepe-el-vande/peer-review-kepangkatan-akademik-siprd/-/blob/staging/siprd-fe/Dockerfile
# and https://nodejs.org/en/docs/guides/nodejs-docker-webapp/

FROM node:14.19.0 AS builder

# Create app directory
WORKDIR /build

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm ci

# Bundle app source
COPY . .

RUN npm run build

FROM nginx:stable-alpine AS production
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=builder /build/dist .
COPY --from=builder /build/nginx.conf /etc/nginx/conf.d
RUN rm /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]