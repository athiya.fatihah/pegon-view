const timeout = 100000;
jest.setTimeout(10000);

beforeAll(async () => {
    await page.goto("http://localhost:3000/transliterator", { waitUntil: "domcontentloaded" });
    await page.waitForSelector('button[id="swapButton"]')
    const swapButton = await page.$('button[id="swapButton"]');
    await swapButton.evaluate(b => b.click());
});

describe("Arab Pegon IME test", () => {
    test("typing pangram", async () => {
        await page.waitForSelector('textarea[id="translit-textarea"]')
        const textArea = await page.$('textarea[id="translit-textarea"]');

        await textArea.evaluate(t => t.focus())
        await page.keyboard.type(`alkisah saya, mu'ad_z, ucup, h_habib s_halih, fad_Hil, sid_harta, ibu indah, z_hahir, dan t_tahir yan_g c^ent_hil k_hawatir lihat foto ham^en_gkubuwono 15 b^ers_yal m^en_y^etir vespa di eropa b^ersama ^enam zebra purba g_haib cantik nan gant^en_g yan_g jatuh dari p^esawat qatar yan_g olen_g saat t_sunami`, {delay: 100});

        const textResult = await textArea.evaluate(t => t.value);
        expect(textResult).toBe(`ألْكِيْسَاهْ سَايَا، مُوْعَاذْ، اُوْچُوْڤْ، حَابِيْبْ صَالِيْهْ، فَاضِيْلْ، سِيْڊَارْتَا، اِيْبُوْ إنْدَاهْ، ظَاهِيْرْ، دَانْ طَاهِيْرْ يَاڠْ چٓنْڟِيْلْ خَاوَاتِيْرْ لِيْهَاتْ فَوْتَوْ هَامٓڠْكُوْبُوْوَوْنَوْ 15 بٓرْشَالْ مٓۑٓتِيْرْ ڤَيْسْڤَا دِىْ اَيْرَوْڤَا بٓرْسَامَا آنَامْ زَيْبْرَا ڤُوْرْبَا غَاهِيْبْ چَانْتِيْكْ نَانْ ࢴَانْتٓڠْ يَاڠْ جَاتُوْهْ دَارِىْ ڤٓسَاوَاتْ قَاتَارْ يَاڠْ اَوْلَيْڠْ سَاأتْ ثُوْنَامِىْ`);
    }, timeout);

    test("Double vowel words test", async () => {
        await page.waitForSelector('textarea[id="translit-textarea"]')
        const textArea = await page.$('textarea[id="translit-textarea"]');
        await textArea.evaluate(t => {t.value = ""})
        await textArea.evaluate(t => t.focus())
        await page.keyboard.type(`gigi dakukir, diukir naas macaa mrenea piatu bantuan m^ertua in_ggih p^etai paido raup piutan_g maesa kae kaos teori ma^em`, {delay: 100});

        const textResult = await textArea.evaluate(t => t.value);
        expect(textResult).toBe(`ࢴِيْࢴِىْ دَاكْ اُوْكِيْرْ، دِيْ اُوْكِيْرْ نَاأسْ مَاچَاهَا مْرَيْنَيْيَا ڤِيَاتُوْ بَانْتُوْاَنْ مٓرْتُوْوَا إڠْࢴِيْهْ ڤٓتَاهِىْ ڤَاهِيْدَوْ رَاهُوْڤْ ڤِيُوْتَاڠْ مَاهَيْسَا كَاهَىْ كَاهَوْسْ تَٸَوْرِىْ مَاٸٓـمْ`);
    }, timeout);

    test("Vocals at the beginning of the word test", async () => {
        await page.waitForSelector('textarea[id="translit-textarea"]')
        const textArea = await page.$('textarea[id="translit-textarea"]');
        await textArea.evaluate(t => {t.value = ""})
        await textArea.evaluate(t => t.focus())
        await page.keyboard.type(`aku ukur ^enam k^eresek otak ini esok`, {delay: 100});

        const textResult = await textArea.evaluate(t => t.value);
        expect(textResult).toBe(`أكُوْ اُوْكُوْرْ آنَامْ كٓرَيْسَيْكْ اَوْتَاكْ اِيْنِىْ اَيْسَوْكْ`);
    }, timeout);
});
