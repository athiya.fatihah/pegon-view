const timeout = 10000;
jest.setTimeout(10000);

beforeAll(async () => {
    await page.goto("http://localhost:3000/docs", { waitUntil: "domcontentloaded" });
});

describe("Docs parts exist", () => {
    test("Search bar exists", async () => {
        const exists = !! await page.$('.searchinput');
        expect(exists).toBe(true);
    }, timeout);
});
