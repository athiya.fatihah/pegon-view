const fs = require('fs');
import { interceptOn, interceptOff, toggleVerbose } from '../utils/intercept'

const timeout = 1000000;
const sleep = ms => new Promise(r => setTimeout(r, ms))
const fileUploadTimeout = 500;

const defaultMessage = 'Belum diimplementasi'
const DEBUG = false;
const pdfInputSelector = 'input[id="pdf-input"][accept="application/pdf"]';
const imgInputSelector = 'input[id="img-input"][accept="image/*"]';
const shortPDFFilename = 'short_filename.pdf';
const shortImgFilename = 'short_filename.png';
const longPDFFilename = 'a_really_really_really_really_long_filename.pdf'
const longImgFilename = 'a_really_really_really_really_long_filename.png';

const uploadEmptyFile = async (filename, inputElement) => {
    fs.closeSync(fs.openSync(filename, 'w'))
    await inputElement.uploadFile(filename);
    fs.unlinkSync(filename);
}

const getElementAsync = async (page, selector) => {
    try {
        await page.waitForSelector(selector, {timeout: timeout});
    } catch (e) {
        if (e instanceof TimeoutError) {
            fail(`Timeout finding selector after ${timeout}ms`)
        }
    }
    const element = await page.$(selector);
    return element;
}

const getInnerHTML = async (page, selector) => {
    const handle = await getElementAsync(page, selector);
    const innerHTML = await page.evaluate(e => e.innerHTML, handle);
    return innerHTML;
}

const getValue = async (page, selector) => {
    const handle = await getElementAsync(page, selector);
    const value = await page.evaluate(e => e.value, handle);
    return value;
}

let watcher;

beforeAll(async () => {
    await page.goto("http://localhost:3000/ocr", { waitUntil: "domcontentloaded" });
    if (DEBUG) {
        toggleVerbose(page);
    }
    await page.setRequestInterception(true);
});

describe("Upload inputs exist", () => {
    test("image input exists", async () => {
        const exists = !! await page.$('input[id="img-input"][accept="image/*"]');
        expect(exists).toBe(true);
    }, timeout);

    test("pdf input exists",  async () => {
        const exists = !! await page.$('input[id="pdf-input"][accept="application/pdf"]');
        expect(exists).toBe(true);
    }, timeout);
});

describe("Uploading images behavior", () => {
    beforeAll(() => {
        watcher = interceptOn(page, "post", '/ocr/img', {
            status: 200,
            headers: {"Access-Control-Allow-Origin": "*"},
            contentType: 'application/json',
            body: JSON.stringify({ result: defaultMessage })
        });
    });
    
    afterAll(() => {
        interceptOff(page, watcher);
    });
    
    test("uploading image works and has result", async () => {
        const input = await getElementAsync(page, imgInputSelector);

        await uploadEmptyFile(shortImgFilename, input);

        const textArea = await getElementAsync(page, textareaSelector);
        const textResult = await textArea.evaluate(t => t.value);
        
        expect(textResult).toEqual(defaultMessage);
    }, timeout);

    test("uploading image changes the short filename", async () => {
        const input = await getElementAsync(page, imgInputSelector);

        await uploadEmptyFile(shortImgFilename, input);
        
        const divText = await getInnerHTML(page, 'label[for="img-input"] div[class="upload-box-text"]');
        expect(divText).toEqual(shortImgFilename);
    }, timeout);

    test("uploading image changes the long filename", async () => {
        const input = await getElementAsync(page, imgInputSelector);

        await uploadEmptyFile(longImgFilename, input);
        
        const divText = await getInnerHTML(page, 'label[for="img-input"] div[class="upload-box-text"]');
        expect(divText).toEqual(longImgFilename.substring(0, 17).concat("...png"));
    }, timeout);
});

describe("Uploading pdf behavior", () => {
    beforeAll(() => {
        watcher = interceptOn(page, "post", '/ocr/pdf', {
            status: 200,
            headers: {"Access-Control-Allow-Origin": "*"},
            contentType: 'application/json',
            body: JSON.stringify({ result: defaultMessage })
        });
    });
    
    afterAll(() => {
        interceptOff(page, watcher);
    });

    test("uploading pdf works and has result", async () => {
        const input = await getElementAsync(page, pdfInputSelector);

        await uploadEmptyFile(shortPDFFilename, input);

        const textResult = await getValue(page, textareaSelector);
        
        expect(textResult).toEqual(defaultMessage);
    }, timeout);
    
    test("uploading pdf changes the short filename", async () => {
        
        const input = await getElementAsync(page, pdfInputSelector);

        await uploadEmptyFile(shortPDFFilename, input);
        
        const divText = await getInnerHTML(page, 'label[for="pdf-input"] div[class="upload-box-text"]');
        expect(divText).toEqual(shortPDFFilename);
    }, timeout);

    test("uploading pdf changes the long filename", async () => {
        const input = await getElementAsync(page, pdfInputSelector);

        await uploadEmptyFile(longPDFFilename, input);

        const divText = await getInnerHTML(page, 'label[for="pdf-input"] div[class="upload-box-text"]');
        expect(divText).toEqual(longPDFFilename.substring(0, 17).concat("...pdf"));
    }, timeout);    
});

const setResponse = (page, method, url, status, body) => {
    return interceptOn(page, method, url, {
        status: status,
        headers: {"Access-Control-Allow-Origin": "*"},
        contentType: 'text/plain',
        body: body,
    });
}

let previousMessage;
const textareaSelector = 'textarea';

describe("Backend fails inform correctly", () => {
    beforeEach(async () => {
        previousMessage = await getValue(page, textareaSelector);
    });
    
    afterEach(async () => {
        const errorDiv = await getElementAsync(page, "#error-msg");
        await page.evaluate(e => e.click(), errorDiv);
        interceptOff(page, watcher);
    });

    it("Wrong file input to /img", async () => {
        watcher = setResponse(page, "post", "/ocr/img", 401, "Please provide an image")
        const input = await getElementAsync(page, imgInputSelector);

        await uploadEmptyFile(shortPDFFilename, input);

        const errorMessage = await getInnerHTML(page, '#error-msg')
        const textResult = await getValue(page, textareaSelector);

        expect(errorMessage).toEqual("Mohon berikan file yang benar");
        expect(textResult).toEqual(previousMessage);
    }, timeout);
    
    it("Wrong file input to /pdf", async () => {
        watcher = setResponse(page, "post", "/ocr/pdf", 401, "Please provide a PDF file")
        
        const input = await getElementAsync(page, pdfInputSelector);

        await uploadEmptyFile(shortImgFilename, input);

        const errorMessage = await getInnerHTML(page, '#error-msg')
        const textResult = await getValue(page, textareaSelector);

        expect(errorMessage).toEqual("Mohon berikan file yang benar");
        expect(textResult).toEqual(previousMessage);
    }, timeout);

    it("Server fails", async () => {
        watcher = setResponse(page, "post", "/ocr/pdf", 500, "Server failed")
        
        const input = await getElementAsync(page, pdfInputSelector);

        await uploadEmptyFile(shortPDFFilename, input);

        const errorMessage = await getInnerHTML(page, '#error-msg')
        const textResult = await getValue(page, textareaSelector);

        expect(errorMessage).toEqual("Maaf, ada kesalahan server");
        expect(textResult).toEqual(previousMessage);
    }, timeout);

    it("Unknown failure", async () => {
        watcher = setResponse(page, "post", "/ocr/img", 400, "Bad request")
        const input = await getElementAsync(page, imgInputSelector);

        await uploadEmptyFile(shortImgFilename, input);

        const errorMessage = await getInnerHTML(page, '#error-msg')
        const textResult = await getValue(page, textareaSelector);

        expect(errorMessage).toEqual("Maaf, terjadi kesalahan yang tidak diketahui");
        expect(textResult).toEqual(previousMessage);
    }, timeout);

});

describe("Buttons work", () => {
    test("Download button works", async () => {
        const downloadPath = './downloads'
        await page._client.send('Page.setDownloadBehavior',
                                {behavior: 'allow',
                                 downloadPath: downloadPath})


        const input = await getElementAsync(page, 'input#txt-export[type="button"]')
        await input.evaluate(b => b.click())

        await page.waitForNetworkIdle()

        const fileNames = fs.readdirSync(downloadPath);
        const fileContent = fs.readFileSync(`${downloadPath}/${fileNames[0]}`,
                                            {encoding: "utf8"});

        fs.unlinkSync(`${downloadPath}/${fileNames[0]}`);
        
        expect(fileContent).toEqual(previousMessage);
    }, timeout * 3);

});
