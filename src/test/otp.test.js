const timeout = 10000;
jest.setTimeout(10000);

beforeAll(async () => {
    await page.goto("http://localhost:3000/otp", { waitUntil: "domcontentloaded" });
});

describe("All important Register Page parts exist", () => {
    test("OTP input exists", async () => {
        const exists = !! await page.$('input[id="otp-input"]');
        expect(exists).toBe(true);
    },timeout)

});
