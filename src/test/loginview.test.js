const timeout = 10000;
jest.setTimeout(10000);

beforeAll(async () => {
    await page.goto("http://localhost:3000/login", { waitUntil: "domcontentloaded" });
});

describe("All important Login Page parts exists", () => {
    test("Email input exists", async () => {
        const exists = !! await page.$('input[id="input-email"]');
        expect(exists).toBe(true);
    },timeout)

    test("Password input exists", async () => {
        const exists = !! await page.$('input[id="input-password"]');
        expect(exists).toBe(true);
    },timeout)

    test("Register button exists", async () => {
        const exists = !! await page.$('button[id="btn-regis"]');
        expect(exists).toBe(true);
    },timeout)

    test("Right side exists", async () => {
        const exists = !! await page.$('div[id="right-side"]');
        expect(exists).toBe(true);
    },timeout)
});
