const timeout = 10000;
jest.setTimeout(10000);

beforeAll(async () => {
    await page.goto("http://localhost:3000/register", { waitUntil: "domcontentloaded" });
});

describe("All important Register Page parts exist", () => {
    test("Nama Lengkap input exists", async () => {
        const exists = !! await page.$('input[id="input-nama"]');
        expect(exists).toBe(true);
    },timeout)

    test("Nomor Telepon input exists", async () => {
        const exists = !! await page.$('input[id="input-notelp"]');
        expect(exists).toBe(true);
    },timeout)

    test("Email input exists", async () => {
        const exists = !! await page.$('input[id="input-email"]');
        expect(exists).toBe(true);
    },timeout)

    test("Password input exists", async () => {
        const exists = !! await page.$('input[id="input-password"]');
        expect(exists).toBe(true);
    },timeout)

    test("Right side exists", async () => {
        const exists = !! await page.$('div[id="right-side"]');
        expect(exists).toBe(true);
    },timeout)
});
