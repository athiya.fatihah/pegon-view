import type { ResponseForRequest, Page, ConsoleMessage, HTTPRequest, HTTPResponse, Dialog, Frame, WebWorker, Metrics } from 'puppeteer'


type EventString = 'close'
    | 'console'
    | 'dialog'
    | 'domcontentloaded'
    | 'error'
    | 'frameattached'
    | 'framedetached'
    | 'framenavigated'
    | 'load'
    | 'metrics'
    | 'pageerror'
    | 'popup'
    | 'request'
    | 'requestfailed'
    | 'requestfinished'
    | 'response'
    | 'workercreated'
    | 'workerdestroyed'

type Event = Error | ConsoleMessage | Dialog | Frame | Page | HTTPRequest | HTTPResponse | { title: string; metrics: Metrics; } | WebWorker

type EventLogger<T> = (event: T) => void

type Watcher = [EventString, EventLogger<Event>]

let isVerbose = false

const logger = <T extends Event>(formatString: string) => {
    return (event: Event) => {
        event = <T>event
        console.log(eval("`"+formatString+"`"))
    }
}

const verboseWatchers: Watcher[] = [
    ['console',
     logger<ConsoleMessage>('${event.type().toUpperCase()} ${event.text()}')],
    ['request',
     logger<HTTPRequest>('>> ${event.method()}, ${event.url()}')],
    ['response',
     logger<HTTPResponse>('<< ${event.status()}, ${event.url()}')],
    ['pageerror',
     logger<Error>('${event.message}')],
    ['requestfailed',
     logger<HTTPRequest>('${event.failure()?.errorText} ${event.url()}')]
]

// reference: https://stackoverflow.com/a/48365725
export const interceptOn = (page: Page, method: string,  urlPattern: string | RegExp, responseObject: Partial<ResponseForRequest>): Watcher => {
    const func = (request: Event) => {
        request = request as HTTPRequest
        if (method.toUpperCase() == request.method().toUpperCase() &&
            new RegExp(urlPattern).test(request.url()) &&
            !request.response()) {
            request.respond(responseObject);
        } else {
            request.continue();
        }
    }
    page.on('request', func);
    return ['request', func];
}

export const interceptOff = (page: Page, watcher: Watcher) => {
    page.off(...watcher);
}

export const toggleVerbose = (page: Page) => {
    if (!isVerbose) {
        isVerbose = true
        verboseWatchers.map(watcher => page.on(...watcher))
    } else {
        isVerbose = false
        verboseWatchers.map(watcher => page.off(...watcher))
    }
}
