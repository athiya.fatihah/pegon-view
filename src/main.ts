import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import VueCookies from 'vue3-cookies'
import './index.css'
import 'animate.css'

const app = createApp(App)
app.use(VueCookies, {
    expireTimes: "14d",
    path: "/",
    domain: "",
    secure: true,
    sameSite: "None"
});


app.use(router)

app.mount('#app')
