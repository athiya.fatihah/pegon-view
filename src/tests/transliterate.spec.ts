/// <reference types="jest" />
import { transliterateLatinToPegon} from '../transliterator/transliterate'
import { transliteratePegonToLatin} from '../transliterator/transliterate'
import { transliterateReversibleLatinToStandardLatin } from '../transliterator/transliterate'
import { transliterate } from '../transliterator/transliterate'
import { makeTransitive, asWordEnding, asWordBeginning, asSingleWord } from '../transliterator/transliterate'
import type { Transliteration, PlainTransliteration } from '../transliterator/transliterate'

describe('Test for Pegon IME', function () {
    it('can make transliteration rules transitive', function () {
        expect(makeTransitive(
            [["t_s", "ث"], ["n_g", "ڠ"], ["p", "ڤ"],
             ["a", "أ"], ["pa", "ڤا"], ["c", "چ"]],
            [["t_sa", "ثا"], ["n_gi", "ڠى"], ["ar", "أرْ"],
              ["ce", "چَى"]],
            [["t_sai", "ثاٸى"], ["n_gid_z", "ڠيذْ"],
             ["aru", "أرو"], ["c^e", "چٓ"]]).sort())
            .toEqual([["t_s", "ث"], ["n_g", "ڠ"], ["p", "ڤ"],
                      ["a", "أ"], ["c", "چ"], ["pa", "ڤا"], ["ثa", "ثا"],
                      ["ڠi", "ڠى"], ["أr", "أرْ"], ["چe", "چَى"],
                      ["ثاi", "ثاٸى"], ["ڠىd_z", "ڠيذْ"],
                      ["أرْu", "أرو"], ["چ^e", "\u0686\u0653"]].sort())
    })
})

describe('Test for Latin to Pegon Transliterator', function () {

    it('can transliterate that follows rule 1', function() {
        expect(transliterateLatinToPegon(
            'b'
        )).toEqual(
            'ب'
        )
    })

    it('can transliterate that follows rule 3', function(){
        expect(transliterateLatinToPegon(
            'mbuh'
        )).toEqual(
            'مبوه'
        )
    })

    it('can transliterate that follows rule 4', function () {
        expect(transliterateLatinToPegon(
            'jawa'
        )).toEqual(
            'جاوا'
        )
    })   
    
    it('can transliterate that follows rule 4 with second option', function() {
        expect(transliterateLatinToPegon(
            'jAwa'
        )).toEqual(
            'جاوا'
        )
    })
    it('can transliterate that follows rule 6 and rule 5', function () {
        expect(transliterateLatinToPegon(
            'nulis'
        )).toEqual(
            'نوليس'
        )
    })

    it('can transliterate that follows rule 5 with second option', function () {
        expect(transliterateLatinToPegon(
            'nulYs'
        )).toEqual(
            'نوليس'
        )
    })

    it('can transliterate that follows rule 6 with second option', function () {
        expect(transliterateLatinToPegon(
            'nWlis'
        )).toEqual(
            'نوليس'
        )
    })


    it('can transliterate that follows rule 7', function() {
        expect(transliterateLatinToPegon(
            'k^epriben'
        )).toEqual(
            'كٓڤريبَين'
        )
    })
    
    it('can transliterate that follows rule 8', function(){
        expect(transliterateLatinToPegon(
            'rod_ha'
        )).toEqual(
            'رَوڊا'
        )
    })
    it('can transliterate that follows rule 9', function(){
      expect(transliterateLatinToPegon(
        't_s^elat_sa'
      )).toEqual(
        'ثٓلاثا'
      )  
    })
    
    
    // Rule 19
    it('can transform single "a" vocal at the front of word', function () {
        expect(transliterateLatinToPegon(
            `aku`
        )).toEqual("أكو")
    })

    // Rule 20
    it('can transform single "a" vocal in the middle where "a" vocal is preceded by "a" vocal', function () {
        expect(transliterateLatinToPegon(
            `nAas`
        )).toEqual("ناأس")
    })

    // RUle 21 Jawa
    it('can transform single "a" vocal in the middle where "a" vocal is preceded by "i" vocal (pegon jawa)', function () {
        expect(transliterateLatinToPegon(
            `pYatu`
        )).toEqual("ڤيياتو")
    })

    // Rule 21 Non-Jawa
    it('can transform single "a" vocal in the middle where "a" vocal is preceded by "i" vocal (pegon non jawa)', function () {
        expect(transliterateLatinToPegon(
            `pY\`atu`
        )).toEqual("ڤيٸاتو")
    })

    // Rule 22
    it('can transform word with suffix "an"', function () {
        expect(transliterateLatinToPegon(
            `bantuan`
        )).toEqual("بانتوان")
    })

    // Rule 23
    it('can transform single "a" vocal at the end of word where "a" vocal is preceded by "a" vocal', function () {
        expect(transliterateLatinToPegon(
            `macaa`
        )).toEqual("ماچاها")
    })

    // Rule 24
    it('can transform single "a" vocal at the end of word where "a" vocal is preceded by "o" vocal', function () {
        expect(transliterateLatinToPegon(
            `mronoa`
        )).toEqual("مرَونَوها")
    })

    // Rule 25
    it('can transform single "a" vocal at the end of word where prev(kasra) or prev(pepet) + voc("a") at the end word', function () {
        expect(transliterateLatinToPegon(
            `w^edia`
        )).toEqual("وٓدييا")
    })

    // RUle 26
    it('can transform single "a" vocal at the end of word where "a" vocal is preceded by "^e" vocal', function () {
        expect(transliterateLatinToPegon(
            `mr^en^ea`
        )).toEqual("مرَينَييا")
    })

    // Rule 27
    it('can transform "a" in the end of a word that is preceded by a syllabel with "u" vocal syllable to a وا', async function () {
        expect(transliterateLatinToPegon(
            `m^ertua`
        )).toEqual("مٓرتووا")
    })

    // Rule 29
    it('can transform "i" in the beginning of a word to ايـ if it is followed by a not dead syllable', function () {
        expect(transliterateLatinToPegon(
            `iki`
        )).toEqual("ايكي")
    })

    // Rule 28
    it('can transform "i" in the beginning of a word to إ if it is followed by a dead syllable', function () {
        expect(transliterateLatinToPegon(
            `in_ggih`
        )).toEqual("إڠڮيه")
    })

    // Rule 30 Jawa
    it('can transform "i" that is preceded by an "a" vocal syllable to هي', function () {
        expect(transliterateLatinToPegon(
            `senpai`
        )).toEqual("سَينڤاهي")
    })

    // Rule 30 Non-Jawa
    it('can transform "i" that is preceded by an "a" vocal syllable to ٸي', function () {
        expect(transliterateLatinToPegon(
            `senpa\`i`
        )).toEqual("سَينڤاٸي")
    })

    // Rule 31
    it('can transform "u" at the beginning of a word to او', function () {
        expect(transliterateLatinToPegon(
            `usap`
        )).toEqual("اوساڤ")
    })

    // Rule 32 Jawa
    it('can transform "u" that is preceded by an "a" vocal syllable to هو', function () {
        expect(transliterateLatinToPegon(
            `raup`
        )).toEqual("راهوڤ")
    })

    // Rule 32 Non-Jawa
    it('can transform "u" that is preceded by an "a" vocal syllable to او', function () {
        expect(transliterateLatinToPegon(
            `raUp`
        )).toEqual("رااوڤ")
    })

    // Rule 33 Jawa
    it('can transform "u" that is preceded by an "i" vocal syllable to يو', function () {
        expect(transliterateLatinToPegon(
            `piutan_g`
        )).toEqual("ڤييوتاڠ")
    })

    // Rule 33 Non-Jawa
    it('can transform "u" that is preceded by an "i" vocal syllable to ٸو', function () {
        expect(transliterateLatinToPegon(
            `pi\`utan_g`
        )).toEqual("ڤيٸوتاڠ")
    })

    // Rule 34
    it('can transform "e" at the beginning of a word to a اَيـ', function () {
        expect(transliterateLatinToPegon(
            `edi`
        )).toEqual("اَيدي")
    })
// Rule 35
    it('can transform "e" that is preceded by an "a" vocal syllable to هَي', function () {
        expect(transliterateLatinToPegon(
            `maesa kae`
        )).toEqual("ماهَيسا كاهَي")
    })

    it('can transform "`e" that is preceded by an "a" vocal syllable to ٸَي', function () {
        expect(transliterateLatinToPegon(
            `ma\`esa ka\`e`
        )).toEqual("ماٸَيسا كاٸَي")
    })
// Rule 36
    it('can transform singular "o" at the beginning of a word to اَو', function () {
        expect(transliterateLatinToPegon(
            `obat`
        )).toEqual("اَوبات")
    })
// Rule 37
    it('can transform "o" that is preceded by an "a" vocal syllable to هَوْ', function () {
        expect(transliterateLatinToPegon(
            `n_gaos`
        )).toEqual("ڠاهَوس")
    })

    it('can transform "O" that is preceded by an "a" vocal syllable to اَو', function () {
        expect(transliterateLatinToPegon(
            `n_gaOs`
        )).toEqual("ڠااَوس")
    })
// Rule 38
    it('can transform "o" that is preceded by an "i" vocal syllable to ٸَو', function () {
        expect(transliterateLatinToPegon(
            `biologi`
        )).toEqual("بيٸَولَوڮي")
    })
// Rule 39
    it('can transform "o" that is preceded by an "e" vocal syllable to ٸَو', function () {
        expect(transliterateLatinToPegon(
            `teori`
        )).toEqual("تَيٸَوري") 
    })
// Rule 40
    it('can transform "e" pepet at the beginning of a word to اࣤ', function () {
        expect(transliterateLatinToPegon(
            `^emoh`   
        )).toEqual("آمَوه")
    })
// Rule 41
    it('can transform "e" pepet in the middle of a word to ٸࣤــ', function () {
        expect(transliterateLatinToPegon(
            `ma^em`   
        )).toEqual("ماٸٓم")
    })

})

describe('Test for Pegon to Latin Transliterator', function () {

    it('can transliterate that follows rule 1', function() {
        expect(transliteratePegonToLatin(
            'ب'
        )).toEqual(
            'b'
        )
    })

    it('can transliterate that follows rule 3', function(){
        expect(transliteratePegonToLatin(
            'مبوه'
        )).toEqual(
            'mbuh'
        )
    })

    it('can transliterate that follows rule 4', function () {
        expect(transliteratePegonToLatin(
            'جاوا'
        )).toEqual(
            'jawa'
        )
    })   
    
    it('can transliterate that follows rule 6 and rule 5', function () {
        expect(transliteratePegonToLatin(
            'نوليس'
        )).toEqual(
            'nulis'
        )
    })


    it('can transliterate that follows rule 7', function() {
        expect(transliteratePegonToLatin(
            'كٓڤريبَين'
        )).toEqual(
            'k^epriben'
        )
    })
    
    it('can transliterate that follows rule 8', function(){
        expect(transliteratePegonToLatin(
            'رَوڊا'
        )).toEqual(
            'rod_ha'
        )
    })

    it('can transliterate that follows rule 9', function(){
      expect(transliteratePegonToLatin(
        'ثٓلاثا'
      )).toEqual(
        't_s^elat_sa'
      )  
    })
    // Rule 19
    it('It can transform a stand-alone أ in the front of a word to "a" ', function () {
        expect(transliteratePegonToLatin(
            `أكو`
        )).toEqual("aku")
    })

    // Rule 20
    it('It can transform a stand-alone أ in the middle of a word to be double "a" ', function () {
        expect(transliteratePegonToLatin(
            `ناأس`
        )).toEqual("nAas")
    })
    
    // Rule 21 Jawa
    it('can transform (CON) + "يا" + Yeh in the middle of a word to "Y + a + (CON)"', function () {
        expect(transliteratePegonToLatin(
            `ڤيياتو`
        )).toEqual("pYatu")
    })
    
    // Rule 21 Non-Jawa
    it('can transform (CON) + "ٸا" + Yeh in the middle of a word to "Y + `a + (CON)"', function () {
        expect(transliteratePegonToLatin(
            `ڤيٸاتو`
        )).toEqual("pY`atu")
    })

    // Rule 22
    it('can transform "ان" + <kt. Dasar> at the end of a word to a <kt. Dasar> + "an"', function () {
        expect(transliteratePegonToLatin(
            `بانتوان`
        )).toEqual("bantuan")
    })

    // Rule 23
    it('can transform "ها" + Alef in the end of a word to "a" + "a"', function () {
        expect(transliteratePegonToLatin(
            `ماچاها`
        )).toEqual("macaa")
    })

    // Rule 24
    it('can transform "ها" + TalingTarung in the end of a word to "o" + "a"', function () {
        expect(transliteratePegonToLatin(
            `مرَونَوها`
        )).toEqual("mronoa")
    })

    // Rule 25
    it('can transform "يا" + Yeh at the end of word to "i" + "a"', function () {
        expect(transliteratePegonToLatin(
            `وٓدييا`
        )).toEqual("w^edia")
    })

    // Rule 26
    it('can transform "يا" + Pepet at the end of word to "^e" + "a"', function () {
        expect(transliteratePegonToLatin(
            `مرَينَييا`
        )).toEqual("mr^en^ea")
    })

    // Rule 27
    it('can transform وا in the end of a word that is preceded by a syllable with "u" vocal syllable to "a"', function () {
        expect(transliteratePegonToLatin(
            `مٓرتووا`
        )).toEqual("m^ertua")
    })

    // Rule 29
    it('can transform ايـ in the beginning of a word to "i"', function () {
        expect(transliteratePegonToLatin(
            `ايكي`
        )).toEqual("iki")
    })

    // Rule 28
    it('can transform إ at the beginning of a word to "i"', function () {
        expect(transliteratePegonToLatin(
            `إڠڮيه`
        )).toEqual("in_ggih")
    })

    // Rule 30 Non-Jawa
    it('can transform ٸي that is preceded by a syllable with "a" vocal syllable to "i"', function () {
        expect(transliteratePegonToLatin(
            `سَينڤاٸي`
        )).toEqual("senpa`i")
    })

    // RUle 30 Jawa
    it('can transform هي that is preceded by a syllable with "a" vocal syllable to "i"', function () {
        expect(transliteratePegonToLatin(
            `سَينڤاهي`
        )).toEqual("senpai")
    })

    // Rule 31
    it('can transform او at the beginning of a word to "u"', function () {
        expect(transliteratePegonToLatin(
            `اوساڤ`
        )).toEqual("usap")
    })

    // Rule 32 Non-Jawa
    it('can transform او at the middle of a word as "u"', function () {
        expect(transliteratePegonToLatin(
            `رااوڤ`
        )).toEqual("raUp")
    })

    // Rule 32 Jawa
    it('can transform هو at the middle of a word as "u"', function () {
        expect(transliteratePegonToLatin(
            `راهوڤ`
        )).toEqual("raup")
    })

    // Rule 33 Non-Jawa
    it('can transform ٸو that is preceded by an "i" vocal syllable to "u"', function () {
        expect(transliteratePegonToLatin(
            `ڤيٸوتاڠ`
        )).toEqual("pi`utan_g")
    })

    // Rule 33 Jawa
    it('can transform يو that is preceded by an "i" vocal syllable to "u"', function () {
        expect(transliteratePegonToLatin(
            `ڤييوتاڠ`
        )).toEqual("piutan_g")
    })

    // Rule 34
    it('can transform اَيـ at the beginning of a word to "e"', function () {
        expect(transliteratePegonToLatin(
            `اَيدي`
        )).toEqual("edi")
    })
// Rule 35
    it('can transform ٸَي at the middle of a word as "e" ', function () {
        expect(transliteratePegonToLatin(
            `ماٸَيسا كاٸَي`
        )).toEqual("ma\`esa ka\`e")
    })

    it('can transform هَي at the middle or end of a word that is as "e" ', function () {
        expect(transliteratePegonToLatin(
            `ماهَيسا كاهَي`
        )).toEqual("maesa kae")
    })
// Rule 36
    it('can transform اَو at the start of a word as "o" ', function () {
        expect(transliteratePegonToLatin(
            `اَوبات`
        )).toEqual("obat")
    })
// Rule 37
    it('can transform اَو that is preceded by an "a" vocal syllable to "o"', function () {
        expect(transliteratePegonToLatin(
            `ڠااَوس`
        )).toEqual("n_gaOs")
    })

    it('can transform هَو that is preceded by an "a" vocal syllable to "o"', function () {
        expect(transliteratePegonToLatin(
            `ڠاهَوس`
        )).toEqual("n_gaos")
    })
// Rule 38
    it('can transform ٸَو that is preceded by an "i" vocal syllable to "o"', function () {
        expect(transliteratePegonToLatin(
            `بيٸَولَوڮي`
        )).toEqual("biologi")
    })
// Rule 39
    it('can transform ٸَو that is preceded by an "e" vocal syllable to "o"', function () {
        expect(transliteratePegonToLatin(
            `تَيٸَوري`
        )).toEqual("teori")
    })
// Rule 40
    it('can transform اࣤ at the start of a word to "e" pepet', function () {
        expect(transliteratePegonToLatin(
            `آمَوه`
        )).toEqual("^emoh")
    })
// Rule 41
    it('can transform ٸࣤــ at the middle of a word to "e" pepet', function () {
        expect(transliteratePegonToLatin(
            `ماٸٓم`
        )).toEqual("ma^em")
    })
})

describe('Test for Reversible Latin to Standard Latin Transliteration', function () {
    it('can transliterate Reversible Latin pangram to Standard Latin', function () {
        expect(transliterateReversibleLatinToStandardLatin(
            `alkisah saya, mu'ad_z, ucup, ad_li, h_habib s_halih, fad_Hil, sid_harta, ibu indah, z_hahir, T_hariq, dan t_tahir yan_g c^ent_hil k_hawatir lihat foto ham^en_gkubuwono 15 b^ers_yal m^en_y^etir vespa di eropa b^ersama ^enam zebra purba g_hayb cantik nan gant^en_g yan_g jatuh dari p^esawat qatar yan_g olen_g saAt t_sunami dan t^erdampar di pulaU seribu. Soren_ya kami ma\`en g_hundu dit^emani kopi pa\`it dan kaOs baru yan_g dib^eli \`a rafi saAt gajYan lalu jual motor mi\`o untuk bayar utan_g pi\`utan_g k^e n_ya_i e_uis yan_g s^edan_g Gala_u`
        )).toEqual("alkisah saya, mu'aẑ, ucup, aḍi, ḥabib ṣalih, fadhil, sidharta, ibu indah, ẓahir, thariq, dan ṭahir yang cênthil khawatir lihat foto hamêngkubuwono 15 bêrsyal mênyêtir vespa di eropa bêrsama ênam zebra purba gayb cantik nan gantêng yang jatuh dari pêsawat qatar yang oleng saat ṡunami dan têrdampar di pulau seribu. Sorenya kami maen gundu ditêmani kopi pait dan kaos baru yang dibêli a rafi saat gajian lalu jual motor mio untuk bayar utang piutang kê nyai euis yang sêdang galau")
    })
})

describe("Test for regex mappers", function () {
    let mockRule: PlainTransliteration[];
    beforeAll(() => {
        mockRule = [
            ["ci", "چىْ"],
            ["e", "هَىْ"],
            ["a", "أ"]
        ]})

        function ruleTester(mocker: Transliteration[], testcase: string) {
            for (let i = 0; i < mocker.length; i ++) {
                if (new RegExp(mocker[i][0], "g").test(testcase)) {
                    return true
                }
            }
            return false
        }

    it("does not duplicate rules", function () {
        expect(asWordBeginning(mockRule).length).toEqual(mockRule.length)
        expect(asWordEnding(mockRule).length).toEqual(mockRule.length)
    })
    
    it("can use rules to detect preceding spaces", function () {
        const mockBeginningResult = asWordBeginning(mockRule)
        
        expect(ruleTester(mockBeginningResult, "makan saْڤi di ciتَاyem")).toBe(true)
        expect(ruleTester(mockBeginningResult, "wah aدَا bule nih")).toBe(true)
        expect(ruleTester(mockBeginningResult, "makan curut bersama مٓرْتُوْa")).toBe(false)
        expect(ruleTester(mockBeginningResult, "tapak سُوْci")).toBe(false)    
    })

    it("can use rules to detect preceding punctuation", function () {
        const mockBeginningResult = asWordBeginning(mockRule)
        
        expect(ruleTester(mockBeginningResult, "makan saْڤi di,ciتَاyem")).toBe(true)
        expect(ruleTester(mockBeginningResult, "wah!aدَا bule nih")).toBe(true)
        expect(ruleTester(mockBeginningResult, "wah?aدَا bule nih")).toBe(true)
        expect(ruleTester(mockBeginningResult, "wah.aدَا bule nih")).toBe(true)
        expect(ruleTester(mockBeginningResult, "makan saڤi di (ciتَاyem)")).toBe(true)
        expect(ruleTester(mockBeginningResult, "makan bareng ciچِىْ-ciْچِى")).toBe(true)
    })

    it("can transliterate in the begining", function() {
        const mockBeginningResult = asWordBeginning(mockRule)
        
        expect(transliterate("makan saْڤi di ciتَاyem", mockBeginningResult))
            .toEqual("makan saْڤi di چىْتَاyem")
        expect(transliterate("makan saْڤi di,ciتَاyem", mockBeginningResult))
            .toEqual("makan saْڤi di,چىْتَاyem")
        expect(transliterate("wah!aدَا bule nih", mockBeginningResult))
            .toEqual("wah!أدَا bule nih")
        expect(transliterate("wah?aدَا bule nih", mockBeginningResult))
            .toEqual("wah?أدَا bule nih")
        expect(transliterate("wah.aدَا bule nih", mockBeginningResult))
            .toEqual("wah.أدَا bule nih")
        expect(transliterate("makan saْڤi di (ciتَاyem)", mockBeginningResult))
            .toEqual("makan saْڤi di (چىْتَاyem)")
        expect(transliterate("makan bareng ciچى-ciچى", mockBeginningResult))
            .toEqual("makan bareng چىْچى-چىْچى")

    })
    
    it("can use rules to detect ending spaces", function () {
        const mockEndingResult = asWordEnding(mockRule)

        expect(ruleTester(mockEndingResult, "چُوْci baju di citayem")).toBe(true)
        expect(ruleTester(mockEndingResult, "ono bule كَاe")).toBe(true)
        expect(ruleTester(mockEndingResult, "ciبَاduyut ciنَا")).toBe(false)
        expect(ruleTester(mockEndingResult, "ehhhh ini ciچَاكْ")).toBe(false)
    })

    it("can use rules to detect ending punctuation", function () {
        const mockEndingResult = asWordEnding(mockRule)

        expect(ruleTester(mockEndingResult, "چُوْci? baju di citayem")).toBe(true)
        expect(ruleTester(mockEndingResult, "ono bule كَاe!")).toBe(true)
        expect(ruleTester(mockEndingResult, "ono bule كَاe.")).toBe(true)
        expect(ruleTester(mockEndingResult, "چُوْci, baju di citayem")).toBe(true)
        expect(ruleTester(mockEndingResult, "(چُوْci) baju di citayem")).toBe(true)
        expect(ruleTester(mockEndingResult, "\"چُوْci\"baju di citayem")).toBe(true)
        expect(ruleTester(mockEndingResult, "چُوْci-چُوْci baju di citayem")).toBe(true)
    })

    it("can transliterate at the end", function () {
        const mockEndingResult = asWordEnding(mockRule)

        expect(transliterate("چُوْci baju di citayem", mockEndingResult))
            .toEqual("چُوْچىْ baju di citayem")
        expect(transliterate("چُوْci? baju di citayem", mockEndingResult))
            .toEqual("چُوْچىْ? baju di citayem")
        expect(transliterate("ono bule كَاe!", mockEndingResult))
            .toEqual("ono bulهَىْ كَاهَىْ!")
        expect(transliterate("ono bule كَاe.", mockEndingResult))
            .toEqual("ono bulهَىْ كَاهَىْ.")
        expect(transliterate("چُوْci, baju di citayem", mockEndingResult))
            .toEqual("چُوْچىْ, baju di citayem")
        expect(transliterate("(چُوci) baju di citayem", mockEndingResult))
            .toBe("(چُوچىْ) baju di citayem")
        expect(transliterate("\"چُوْci\" baju di citayem", mockEndingResult))
            .toBe("\"چُوْچىْ\" baju di citayem")
        expect(transliterate("چُوْci-چُوْci baju di citayem", mockEndingResult))
            .toBe("چُوْچىْ-چُوْچىْ baju di citayem")
    })
})
