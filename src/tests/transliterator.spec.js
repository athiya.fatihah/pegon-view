const timeout = 10000;
const fs = require('fs');

beforeAll(async () => {
    await page.goto("http://localhost:3000/transliterator", { waitUntil: "domcontentloaded" });
    await page.setDefaultNavigationTimeout(timeout);
});

describe("Test literation", () => {
    test("Lit name", async () => {
        const pHandle = await page.$('p[id="latin"]');
        const html = await page.evaluate(pHandle => pHandle.innerHTML, pHandle);
        expect(html).toBe("Latin");
    }, timeout);
});

describe("Test literation 2", () => {
    test("Lit name", async () => {
        const pHandle = await page.$('p[id="arab-pegon"]');
        const html = await page.evaluate(pHandle => pHandle.innerHTML, pHandle);
        expect(html).toBe("Arab Pegon");
    }, timeout);
});

describe("Upload TXT button exists", () => {
    test("exists button", async () => {
        const handle = await page.$('label[for="txt-input"]>div#upload-txt-button');
        const html = await page.evaluate(p => p.innerHTML, handle);
        expect(html).toBe("Upload TXT");
    }, timeout);
});

describe("textarea Latin exists", () => {
    test("exists textarea", async () => {
        const pHandle = await page.$('textarea');
        const html = await page.evaluate(pHandle => pHandle.innerHTML, pHandle);
        expect(html).toBe("Masukkan Disini");
    }, timeout);
});

describe("Languange Selection exists", () => {
    test("exists selector", async () => {
        const pageHandle = await page.$('#languageLabel');
        const html = await page.evaluate(p => p.innerHTML, pageHandle);
        expect(html).toBe("Bahasa Indonesia");
    }, timeout);

    test("exists Indonesian language", async () => {
        const buttonSelector = '#languageButtonSelector';
        await page.waitForSelector(buttonSelector);
        await page.hover(buttonSelector);

        const langSelector = '#languageIndonesia';
        await page.waitForSelector(langSelector);
        await page.click(langSelector);
        
        const pageHandle = await page.$('#languageLabel');
        const html = await page.evaluate(p => p.innerHTML, pageHandle);
        expect(html).toBe("Bahasa Indonesia");
    }, timeout);

    test("exists Javanese language", async () => {
        const buttonSelector = '#languageButtonSelector';
        await page.waitForSelector(buttonSelector);
        await page.hover(buttonSelector);

        const langSelector = '#languageJawa';
        await page.waitForSelector(langSelector);
        await page.click(langSelector);
        
        const pageHandle = await page.$('#languageLabel');
        const html = await page.evaluate(p => p.innerHTML, pageHandle);
        expect(html).toBe("Bahasa Jawa");
    }, timeout);

    test("exists Sundanese language", async () => {
        const buttonSelector = '#languageButtonSelector';
        await page.waitForSelector(buttonSelector);
        await page.hover(buttonSelector);

        const langSelector = '#languageSunda';
        await page.waitForSelector(langSelector);
        await page.click(langSelector);
        
        const pageHandle = await page.$('#languageLabel');
        const html = await page.evaluate(p => p.innerHTML, pageHandle);
        expect(html).toBe("Bahasa Sunda");
    }, timeout);

    test("exists Madurese language", async () => {
        const buttonSelector = '#languageButtonSelector';
        await page.waitForSelector(buttonSelector);
        await page.hover(buttonSelector);

        const langSelector = '#languageMadura';
        await page.waitForSelector(langSelector);
        await page.click(langSelector);
        
        const pageHandle = await page.$('#languageLabel');
        const html = await page.evaluate(p => p.innerHTML, pageHandle);
        expect(html).toBe("Bahasa Madura");
    }, timeout);
})

describe("Buttons work", () => {
    test("Upload button works", async () => {
        const txtInputSelector = 'input[id="txt-input"][accept="text/plain"]';
        await page.waitForSelector(txtInputSelector);
        const input = await page.$(txtInputSelector);
        const filename = "input.txt";
        const fileContent = "se_lamat pagi"
        const fd = fs.openSync(filename, 'w')
        fs.writeSync(fd, fileContent)
        fs.closeSync(fd)
        await input.uploadFile(filename);

        const textareaHandle = await page.$('textarea#translit-textarea');
        const text = await page.evaluate(t => t.innerHTML, textareaHandle);
        fs.unlinkSync(filename);
        expect(text).toEqual(fileContent);
    }, timeout);

    test("Download button works", async () => {
        const downloadPath = './downloads'
        await page._client.send('Page.setDownloadBehavior',
                                {behavior: 'allow',
                                 downloadPath: downloadPath})

        const textareaSelector = 'textarea[id="translit-textarea"]'
        await page.waitForSelector(textareaSelector)
        const textArea = await page.$(textareaSelector)
        await textArea.evaluate(t => t.focus())

        const text = "se_lamat pagi"
        await page.keyboard.type(text)
        
        const resultSelector = 'div#left-text-box>.translit-result-text'
        await page.waitForSelector(resultSelector)
        const resultHandle = await page.$(resultSelector)
        const textResult = await resultHandle.evaluate(t => t.innerHTML)
        
        const clickSelector = 'input#txt-export[type="button"]';
        await page.waitForSelector(clickSelector)
        const input = await page.$(clickSelector)
        await input.evaluate(b => b.click())

        await page.waitForNetworkIdle()

        const fileNames = fs.readdirSync(downloadPath);
        const fileContent = fs.readFileSync(`${downloadPath}/${fileNames[0]}`,
                                            {encoding: "utf8"});

        fs.unlinkSync(`${downloadPath}/${fileNames[0]}`);
        
        expect(textResult).toEqual(fileContent);
    }, timeout * 3);

});
