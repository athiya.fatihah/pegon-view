import { createRouter, createWebHistory } from 'vue-router'
import CorpusView from '../views/CorpusView.vue'
import CreateCorpusView from '../views/CreateCorpusView.vue'
import UpdateCorpusView from '../views/UpdateCorpusView.vue'
import DocsView from '../views/DocsView.vue'
import HomepageView from '../views/HomepageView.vue'
import LoginView from '../views/LoginView.vue'
import OCRView from '../views/OCRView.vue'
import OTPView from '../views/OTPView.vue'
import RegisterView from '../views/RegisterView.vue'
import TransliteratorView from '../views/TransliteratorView.vue'
import AdminPanelView from '../views/AdminPanelView.vue'
import UpdatePanelView from '../views/UpdatePanelView.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/transliterator',
      name: 'transliterator',
      component: TransliteratorView
    },
    {
      path: '/ocr',
      name: 'ocr',
      component: OCRView
    },
    {
      path: '/ocr',
      name: 'ocr',
      component: OCRView
    },
    {
      path: '/docs',
      name: 'docs',
      component: DocsView
    },
    {
      path: '/corpus',
      name: 'corpus',
      component: CorpusView
    },
    {
      path: '/corpus/create',
      name: 'createCorpus',
      component: CreateCorpusView
    },
    {
      path: '/corpus/update/:id',
      name: 'updateCorpus',
      component: UpdateCorpusView
    },
    {
      path: '/otp',
      name: 'otp',
      component: OTPView
    },
    {
      path: '/',
      name: 'homepage',
      component: HomepageView
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterView
    },
    {
      path: '/panel',
      name: 'panel',
      component: AdminPanelView
    },
    {
      path: '/panel/update/:id',
      name: 'panel_update',
      component: UpdatePanelView
    },
  ]
})

export default router
